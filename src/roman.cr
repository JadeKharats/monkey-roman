abstract struct Int
  ROMAN_TABLE = {
    1000 => "M",
    900 => "CM",
    500 => "D",
    400 => "CD",
    100 => "C",
    90 => "XC",
    50 => "L",
    40 => "XL",
    10 => "X",
    9 => "IX",
    5 => "V",
    4 => "IV",
    1 => "I"
    }

  def to_roman
    raise TooHighNumberException.new if self > 5000
    raise NegativeNumberException.new if self < 0
    return "" if self == 0
    key = ROMAN_TABLE.keys.select{ |number|  number <= self }.max
    ROMAN_TABLE[key]+(self-key).to_roman
  end

end

class NegativeNumberException < Exception
end
class TooHighNumberException < Exception
end
