require "./spec_helper"

describe Int do
  describe "#to_roman" do

    it "raise an exception if it was superior to 5000" do
      expect_raises(TooHighNumberException) do
        5001.to_roman
      end
    end
    it "raise an exceptionif it was inferior to 0" do
      expect_raises(NegativeNumberException) do
        -1.to_roman
      end
    end
    it "convert 1 to I" do
      1.to_roman.should eq "I"
    end
    it "convert 2 to II" do
      2.to_roman.should eq "II"
    end
    it "convert 4 to IV"  do
      4.to_roman.should eq "IV"
    end
    it "convert 14 to XIV" do
      14.to_roman.should eq "XIV"
    end
    it "convert 2896 to MMDCCCXCVI" do
      2896.to_roman.should eq "MMDCCCXCVI"
    end
  end
end
